import React from "react";
import { Link, Route, Switch, withRouter } from "react-router-dom";

function GalleryContentHeader(title) {
	//var title = useParams();
	return (
          <h2>
              <Link to="/" className="back_icon"></Link>{title}
          </h2>

		);

}

function GalleriesHeader(title) {
	return (
	          <h2>
	              {title}
	          </h2>
		);

}

class Header extends React.Component { 
	state = {};
	constructor(props) {
	   super(props);
	   //this.setState({headerImage:''});
	   
	}
	componentDidUpdate(prevProps) {
		if(prevProps !== this.props){
	        this.setState(this.props);
	    }
		console.log('header state', this.state);
	}
  	render() {
	  	return (
	        <header id="head">
	            <div className="background edges"><div style={{backgroundImage: 'url("'+this.state.headerImage+'")'}}></div></div>

				<div className="container">
						  <h1>
		                      Fotogaléria
		                  </h1>
		                   	<Switch>
					        	<Route path="/" exact>{GalleriesHeader(this.props.title)}</Route>
					        	<Route path="/gallery/:path">{GalleryContentHeader(this.props.title)}</Route>
					      	</Switch>
	             </div>
                

	           
	        </header>
	  	);
	}
}


export default withRouter(Header);