import React from "react";

export default class GalleryImage extends React.Component {
	changeHeader() {
	  const { REACT_APP_IMAGE_API_URL }  = process.env;
	  const imageUrl = REACT_APP_IMAGE_API_URL+'images/263x180/'+this.props.item.fullpath;
	  console.log('change header', imageUrl);
	  this.props.handleChangeState({headerImage:imageUrl});
	}
  	render() {
		const { REACT_APP_IMAGE_API_URL } = process.env;
		return (
		 	<div className="col-md-6 col-lg-4 col-xl-3">
		        <a onMouseEnter={this.changeHeader.bind(this)} className="thumbnail" href="#image" data-toggle="modal" data-image={REACT_APP_IMAGE_API_URL+'images/0x0/'+this.props.item.fullpath} data-image-id="" data-title={this.props.item.name} data-target="#image-gallery">
		            <img src={REACT_APP_IMAGE_API_URL+'images/400x0/'+this.props.item.fullpath} alt={this.props.item.name} />
		        </a>
			</div>
  		);
	}
}



       
