import React from "react";
import $ from 'jquery'; 
import { withRouter } from "react-router-dom";
import GalleryImage from "../components/GalleryImage";
import GalleryImageModal from "../components/modals/GalleryImageModal";
import AddImageModal from "../components/modals/AddImageModal";

class GalleryContent extends React.Component {
   state = {
      images: [],
      path: '',
    };
	constructor(props) {
	   super(props);
	   this.state.path = this.props.match.params.path;
	}

  async loadImages() {
    const { REACT_APP_IMAGE_API_URL }  = process.env;    
    let response = await fetch( REACT_APP_IMAGE_API_URL+"gallery/"+this.state.path);
    let data = await response.json();
    const newState = Object.assign({}, this.state, {
      images: data.images
    });

    this.setState(newState);
    this.props.handleChangeState({title:data.gallery.name});
    this.props.handleChangeState({headerImage:REACT_APP_IMAGE_API_URL+'images/400x0/'+data.gallery.image.fullpath});
    
  }

  createGallery() {
      /*$(function(){
        $('#head .background div').css('background-image', 'url("'+$('.thumbnails img').first().attr('src')+'")');
        $('.thumbnails .thumbnail:not(.add)').mouseover(function(){
          let image = $('img', $(this)).attr('src');
          if(typeof image != 'undefined') {
            console.log('image changed', image);
            $('#head .background div').css('background-image', 'url("'+image+'")'); 
          }
          else {
            
          }
          
          
        });

    });*/

/**
 *
 * GALLERY 
 * 
 */


let modalId = $('#image-gallery');


$(document)
  .ready(function () {
      initGallery();

   });
 

    //This function disables buttons when needed
    function disableButtons(counter_max, counter_current) {
      $('#show-previous-image, #show-next-image')
        .show();
      if (counter_max === counter_current) {
        $('#show-next-image')
          .hide();
      } else if (counter_current === 1) {
        $('#show-previous-image')
          .hide();
      }
    }

    function initGallery() {
      loadGallery(true, 'a.thumbnail');
    }

    /**
     *
     * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
     * @param setClickAttr  Sets the attribute for the click handler.
     */

    function loadGallery(setIDs, setClickAttr) {
      let current_image,
        selector,
        counter = 0;

      $('#show-next-image, #show-previous-image')
        .click(function () {
          if ($(this)
            .attr('id') === 'show-previous-image') {
            current_image--;
          } else {
            current_image++;
          }

          selector = $('[data-image-id="' + current_image + '"]');
          updateGallery(selector);
        });

      function updateGallery(selector) {
        let $sel = selector;
        current_image = $sel.data('image-id');
        $('#image-gallery-title')
          .text($sel.data('title'));
        $('#image-gallery-image')
          .attr('src', $sel.data('image'));
        disableButtons(counter, $sel.data('image-id'));
      }

      if (setIDs === true) {
        $('[data-image-id]')
          .each(function () {
            counter++;
            $(this)
              .attr('data-image-id', counter);
          });
      }
      $(setClickAttr)
        .on('click', function () {
          updateGallery($(this));
        });
    }

// build key actions
$(document)
  .keydown(function (e) {
    switch (e.which) {
      case 37: // left
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-previous-image').is(":visible")) {
          $('#show-previous-image')
            .click();
        }
        break;

      case 39: // right
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-next-image').is(":visible")) {
          $('#show-next-image')
            .click();
        }
        break;

      default:
        return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
  });


/**
 *
 * GALLERY END
 * 
 */





    }


  async refreshGallery() {
     try {
       this.loadImages().then(() => {this.createGallery();})
       
     }  catch(error) {
       console.log(error);
     }

  }

	componentDidMount() {
    this.props.handleChangeState({title:''});

    this.refreshGallery()
    .catch(error => console.log(error));
	}

	getGalleryImages() {
		return  (
  			this.state.images.map(image => <GalleryImage key={image.path} item={image}  handleChangeState={this.props.handleChangeState} />)
		);
		
	}


  	render() {
	  return (
	  	<div className="gallery-page">
	        <div className="container text-center" id="content">
	            <section className="thumbnails row text-center">
	            	{this.getGalleryImages()}
	                <div className="add col-md-6 col-lg-4 col-xl-3">
	                    <div className="thumbnail add">
	                        <a href="#addImageModal" data-toggle="modal" data-target="#addImageModal">
	                            <span className="icon add_photo">
	                            </span>
	                            Pridať fotky
	                        </a>
	                    </div>
	                </div>
	            </section>
	        </div>
		    <GalleryImageModal /> 
		    <AddImageModal path={this.state.path} handlerAddImages={this.refreshGallery.bind(this)} />             	
        </div>
		  );
	}
}

export default withRouter(GalleryContent);

       
