import React from "react";

export default class GalleryImageModal extends React.Component {

  render() {
	  return (
		<div className="modal fade" id="image-gallery" tabIndex="-1" role="dialog" aria-labelledby="gallery" aria-hidden="true">
            <div className="modal-dialog modal-lg  modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close close_icon" data-dismiss="modal" aria-label="Close">
                          Zavrieť
                        </button>
                    </div>
                    <div className="modal-body">
                        <img id="image-gallery-image" className="img-responsive col-md-12" src="" alt="" />
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn float-left" id="show-previous-image">
                        </button>

                        <button type="button" id="show-next-image" className="btn float-right">
                        </button>
                    </div>
                </div>
            </div>
        </div>
		  );
	}
}



       
