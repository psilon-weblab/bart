import React, {useState} from "react";
import {useDropzone} from 'react-dropzone';
import $ from 'jquery'; 

export default function AddImageModal(props) {
  
  const [isUploading, setIsUploading] = useState(false);
  const [filesChanged, setFilesChanged] = useState(false);
  
  const {acceptedFiles, getRootProps, getInputProps} = useDropzone();

  const resetForm = () => {
    console.log(filesChanged);
    console.log('type', typeof filesChanged);
    while(acceptedFiles.length) {
      acceptedFiles.pop();  
    }
    setFilesChanged(!filesChanged);
    
  };

  const getFiles = () => {
      return (
        acceptedFiles.map(file => (
        <li key={file.path}>
          {file.path} - {file.size} bytes
        </li>
        )));  
    }
    
  

    const { REACT_APP_IMAGE_API_URL }  = process.env;
    const upload = () => {
      var formData = new FormData();
  
      acceptedFiles.map((file, index) => {
        formData.append(`file${index}`, file);
        return [];
      });
      
      setIsUploading(true);
      fetch(REACT_APP_IMAGE_API_URL+'gallery/'+props.path, {
        method: 'POST',
        body: formData,
      })
        .then(response => response.json())
        .then(success => {
          props.handlerAddImages();
          $('button.close_icon').click();
          setIsUploading(false);
          

        })
        .catch(error => console.log(error)
      );
    }
  
	  return (
            <div className="modal fade" id="addImageModal" tabIndex="-1" aria-labelledby="addImage" aria-hidden="true">
              <div className="modal-dialog modal-dialog-centered">
                <div className={`modal-content ${isUploading ? "loading" : ""}`}>
                  <div className="modal-header">
                    <h5 className="modal-title" id="addImage">Pridať fotky</h5>
                    <button type="button" className="close close_icon" data-dismiss="modal" aria-label="Close" onClick={resetForm}>
                      Zavrieť
                    </button>
                  </div>
                  <div className="modal-body">
                       


                        <div className="lds-dual-ring"></div>
                        <div className="inputContainer">
                            <div className="row">
                              <div className="col-md-12">
                                    <div className="dropArea">
                                       <section className="container">
                                          <div {...getRootProps({className: 'dropzone'})}>
                                            <input {...getInputProps()} />
                                            <span className="icon add_photo"></span>
                                            <span className="label"><b>SEM PRESUNTE FOTKY</b><br />alebo</span>
                                            <button className="btn">Vyberte súbor</button>
                                          </div>
                                          <aside>
                                            <ul>{getFiles()}</ul>
                                          </aside>
                                        </section>
                                    </div>
                                  
                                  
                              </div>
                            </div>
                            
                           <button type="button" className="btn btn-primary add_icon float-right col-sm-4" onClick={upload}>
                              Pridať
                            </button>
                        </div>


                  </div>
                </div>
              </div>
            </div>
		  );
	
}
<AddImageModal />


       
