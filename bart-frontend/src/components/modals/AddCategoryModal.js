import React  from "react";
import $ from 'jquery'; 


export default class AddCategoryModal extends React.Component {


	constructor(props) {
		super(props);
		this.state = {
	      modal: false
	    };
		this.handleSubmit = this.handleSubmit.bind(this);
	}


	validateInput(data) {		
		let categoryName = data.get('category_name');

		if(categoryName.includes('/')) {
			alert('Názov kategórie nesmie obsahovať lomítko');
			return false;
		}

		return true;
	}

	handleSubmit(event) {
		const { REACT_APP_IMAGE_API_URL }  = process.env;
		event.preventDefault();
		const data = new FormData(event.target);
		if(event.target.checkValidity() && this.validateInput(data)) {
				const requestOptions = {
				        method: 'POST',
				        headers: { 'Content-Type': 'application/json' },
				        body: JSON.stringify({ name: data.get('category_name') })
				    };
				    fetch(REACT_APP_IMAGE_API_URL+'gallery', requestOptions)
				        .then(async response => {
				            const data = await response.json();

				            // check for error response
				            if (!response.ok) {
				                // get error message from body or default to response status
				                const error = (data && data.message) || response.status;
				                return Promise.reject(error);
				            }

				            this.props.handlerAddCategory();
				            $('button.close_icon').click();
				            
				        })
				        .catch(error => {
				            this.setState({ errorMessage: error.toString() });
				            console.error('There was an error!', error);
				        });			
		}

		//console.log('data', data.get('category_name'));
		
		return false;
	}



	render() {

	  return (

			<div className="modal fade" id="addCategoryModal" tabIndex="-1" aria-labelledby="addCategory" aria-hidden="true">
			  <div className="modal-dialog modal-dialog-centered">
			    <div className="modal-content">
			      <div className="modal-header">
			        <h5 className="modal-title" id="addCategory">Pridať kategóriu</h5>
			        <button type="button" className="close close_icon" data-dismiss="modal" aria-label="Close">
			          Zavrieť
			        </button>
			      </div>
			      <div className="modal-body">
			         <form onSubmit={this.handleSubmit}>
				         <div className="inputContainer row">
				           <input type="text" name="category_name" className="col-sm-8" id="add_category_name_input" placeholder="Zadajte názov kategórie" required />
				           <button type="submit" className="btn btn-primary add_icon float-right col-sm-4">
				              Pridať
				            </button>
				         </div>
			         </form>
			      </div>
			    </div>
			  </div>
			</div>

		  );
	}
}



       
