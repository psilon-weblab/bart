import React from "react";
import { Link } from "react-router-dom";

export default class Gallery extends React.Component {

	constructor(props) {
	   super(props);
	   this.state = {
	   		gallery: props.item,
	   		imagesCount: 0
	   }
	}

	async countGalleryImages() {
		const { REACT_APP_IMAGE_API_URL }  = process.env;
		
		let response = await fetch(REACT_APP_IMAGE_API_URL+"gallery/"+this.state.gallery.path);
		let data = await response.json();
	    const newState = Object.assign({}, this.state, {
	      imagesCount: data.images.length
	    });

	    this.setState(newState);

	}


	componentDidMount() {
		this.countGalleryImages()
		.catch(error => console.log(error));
	

	}

	getPhotoCountText(count) {
		if(count < 1 || count > 4) {
			return count+' fotiek';
		}
		if(count === 1) {
			return count+ ' fotka';
		}
		if(count > 1 && count < 5) {
			return count+ ' fotky';
		}
	}

	getGalleryImage() {
		const { REACT_APP_IMAGE_API_URL }  = process.env;
		if(typeof this.state.gallery.image != "undefined" ) {
			return <img src={REACT_APP_IMAGE_API_URL+'images/263x180/'+this.state.gallery.image.fullpath} alt={this.state.gallery.name} />
		}
		return '';
		
	}

	changeHeader() {
	  const { REACT_APP_IMAGE_API_URL }  = process.env;
	  const imageUrl = REACT_APP_IMAGE_API_URL+'images/263x180/'+this.state.gallery.image.fullpath;
	  console.log('change header', imageUrl);
	  this.props.handleChangeState({headerImage:imageUrl});
	}

  	render() {
	  return (
	                <div className="col-md-6 col-lg-4 col-xl-3">
	                    <Link className="thumbnail" to={'gallery/'+this.state.gallery.path} onMouseEnter={this.changeHeader.bind(this)}  >
	                        {this.getGalleryImage()}
	                        <h3 className="title">
	                            {this.state.gallery.name}
	                            <span className="count">
	                                {this.getPhotoCountText(this.state.imagesCount)}
	                            </span>
	                        </h3>
	                    </Link>
	                </div>
	                  	
		  );
	}
}



       
