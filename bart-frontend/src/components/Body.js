import React from "react";
import { Route, Switch } from "react-router-dom";
import Galleries from "../components/Galleries";
import GalleryContent from "../components/GalleryContent";

export default class Body extends React.Component {

  render() {
      return (
      <Switch>
        <Route path="/" exact component={Galleries} />
        <Route path="/gallery/:path" component={GalleryContent} />
      </Switch>
      );
  }
}


