import React from "react";
import $ from 'jquery'; 
import Gallery from "../components/Gallery";
import AddCategoryModal from "../components/modals/AddCategoryModal";

export default class Galleries extends React.Component {
   state = {
      galleries: []
    };
    
	constructor(props) {
	   super(props);
	}


	/*loadGalleries() {
		const { REACT_APP_IMAGE_API_URL }  = process.env;
		this.props.handleChangeState({title:'Kategórie'});
		
		return fetch(REACT_APP_IMAGE_API_URL+"gallery")
		.then((response) => response.json())
		.then(data => {
			const newState = Object.assign({}, this.state, {
				galleries: data.galleries
			});
			this.setState(newState);
		});
	}*/

	async loadGalleries() {
		const { REACT_APP_IMAGE_API_URL }  = process.env;
		this.props.handleChangeState({title:'Kategórie'});
		let response = await fetch(REACT_APP_IMAGE_API_URL+"gallery");
		let data = await response.json();
		const newState = Object.assign({}, this.state, {
			galleries: data.galleries
		});
		this.setState(newState);
		this.props.handleChangeState({headerImage:REACT_APP_IMAGE_API_URL+'images/263x180/'+data.galleries[0].image.fullpath});
	  	/*$(function(){
			$('#head .background div').css('background-image', 'url("'+$('.thumbnails img').first().attr('src')+'")');
	    	$('.thumbnails .thumbnail:not(.add)').mouseover(function(){
		      	let image = $('img', $(this)).attr('src');
				if(typeof image != 'undefined') {
					$('#head .background div').css('background-image', 'url("'+image+'")');	
				}
	    	});

		});*/
	}

	componentDidMount() {
		this.loadGalleries()
		.catch(error => console.log(error));


	}

  galleries() {
  	return  (
  		this.state.galleries.map(gallery => <Gallery key={gallery.path} item={gallery} handleChangeState={this.props.handleChangeState}  />)
	);
  	
  }


  render() {
		  return (
		  	<div className="galleries">
				<div className="container text-center" id="content">
		            <section className="thumbnails row text-center">
		                {this.galleries()}
		                <div className="add col-md-6 col-lg-4 col-xl-3">
		                    <div className="thumbnail add">
		                        <a href="#addCategoryModal" data-toggle="modal" data-target="#addCategoryModal">
		                            <span className="icon add">
		                            </span>
		                            Pridať kategóriu
		                        </a>
		                    </div>
		                </div>
		            </section>
		        </div>   
		        <AddCategoryModal handlerAddCategory={this.loadGalleries.bind(this)} />   
	        </div>    	
		  );
	}
}



       
