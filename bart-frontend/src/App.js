import React from "react";
import "./css/main.css";
import { BrowserRouter } from "react-router-dom";
import Header from "./components/Header";
import Footer from "./components/Footer";
import { Route, Switch } from "react-router-dom";
import Galleries from "./components/Galleries";
import GalleryContent from "./components/GalleryContent";


class App extends React.Component {

  state = {
      title: 'Kategórie'
    };

  changeState(state) {
    this.setState(state);
  }

  render() { 
    return (
      <BrowserRouter>
        <div className="App">
          <Header title = {this.state.title} headerImage={this.state.headerImage} />
          <div className="body">
            <Switch>
              <Route path="/" exact><Galleries handleChangeState={this.changeState.bind(this)} /></Route>
              <Route path="/gallery/:path"><GalleryContent  handleChangeState={this.changeState.bind(this)} /></Route>
            </Switch>
          </div>
          <Footer />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;