<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File;
use Symfony\Component\HttpFoundation\Response;

class ImageModel
{
    use HasFactory;
    public $storagePath = '';

    public function __construct(string $storagePath)
    {
        $this->storagePath = $storagePath;
        $this->Filesystem = new Filesystem();
    }

    public function getObject(\Symfony\Component\Finder\SplFileInfo $imageFile, string $name) {
        return [
            'path' => $imageFile->getRelativePathname(),
            'fullpath' => $name.'/'.$imageFile->getRelativePathname(),
            'name' => $imageFile->getFilenameWithoutExtension(),
            'modified' => date('Y-m-d\TH:i.0O', $imageFile->getCTime())
        ];

        
    }

    public function exists(string $path) {
        return Storage::exists($path); 
    }

    public function delete(string $path) {
        return Storage::delete($path);
    }

    public function getObjectFromPath(string $path) {
        return Storage::get($path);
       // return $this->Filesystem->files(base_path().$this->storagePath.$path);
    }

    public function getImage($path) {
        
        $file = new \Symfony\Component\HttpFoundation\File\File(base_path() . $this->storagePath.$path);
        $response = Response::create(Storage::get($path), 200, ['Content-Type' => $file->getMimeType()]);
        return $response;

    }

    public function getImageLocation($path) {
        return base_path() . $this->storagePath.$path;

    }
}
