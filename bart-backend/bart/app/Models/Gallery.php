<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Filesystem\Filesystem;
use App\Models\ImageModel;


class Gallery 
{
    use HasFactory;
    public $storagePath = '';

    public function __construct(string $storagePath)
    {
        $this->storagePath = $storagePath;
        $this->Filesystem = new Filesystem();
        $this->ImageModel = new ImageModel($storagePath);
    }

    public function getAllGalleries()
    {
        $directories = $this->Filesystem->directories($this->getGalleriesPath());

        $output      = ['galleries' => []];

        if (!empty($directories)) {
            foreach ($directories as $directory) {

                $output['galleries'][] = $this->makeObject($this->Filesystem->basename($directory), true);
            }
        }

        return $output;
    }

    protected function makeObject(string $name, bool $fromPath = false)
    {
        if ($fromPath) {
            $name = urldecode($name);
        }

        $images = $this->getImagesFromGallery($name, $fromPath);
        if(!empty($images[0])) {
            return [
                'path' => $this->makePath($name, false),
                'name' => $name,
                'image' => $images[0]
            ];
            
        }
        return [
            'path' => $this->makePath($name, false),
            'name' => $name,
        ];
        
        
    }

    public function getGallery(string $name, bool $fromPath = false)
    {

        if ($this->exists($name, $fromPath)) {
            return $this->makeObject($name, $fromPath);
        } else {
            return false;
        }
    }

    public function store(string $name)
    {

        $path   = $this->makePath($name);
        $result = $this->Filesystem->makeDirectory($path, 0775);

        if ($result) {
            return $this->getGallery($name);
        } else {
            return false;
        }
    }

    public function exists(string $name, bool $fromPath = false)
    {
        if($fromPath) {
        	$name = urldecode($name);
        }
        $path = $this->makePath($name);
        return $this->Filesystem->exists($path);
    }

    public function makePath(string $name, bool $fullPath = true)
    {
        if ($fullPath) {
            return $this->getGalleriesPath() . urlencode($name);
        } else {
            return urlencode($name);
        }

    }

    protected function getGalleriesPath()
    {
        return base_path() . $this->storagePath;
    }

    public function findGallery(string $path)
    {
    	$gallery = $this->getGallery($path, true);
    	
    	if(empty($gallery)) {
    		return false;
    	} 

    	$output = ['gallery' => $gallery];

    	$output['images'] = $this->getImagesFromGallery($path, true);

        return $output;
    }

    public function getImagesFromGallery(string $name, bool $fromPath = false)
    {
    	if($fromPath) {
    		$name = urldecode($name);
    	}

        $files = $this->Filesystem->files($this->makePath($name));    
    	$images = [];
        $modified = [];

        if(!empty($files)) {
            foreach ($files as $imageFile) {
                $image = $this->ImageModel->getObject($imageFile, $name);
                $images[] = $image;
                $modified[] = $image['modified'];
            }
        }
        
        array_multisort($modified, SORT_DESC, $images);

    	return $images;
    }

    public function upload($files, $path) {
        $galleryPath = $this->makePath($path, true);
        $output = [];
        foreach ($files as $file) {
            $response = $file->store($galleryPath);
            if($response !== false) {
                $output[] = $response;
            }
            else return false;
        }
    }

    public function delete($path) {
        return $this->Filesystem->deleteDirectory($this->makePath($path, true));
    }

}
