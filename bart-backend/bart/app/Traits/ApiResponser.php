<?php

namespace App\Traits;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\LengthAwarePaginator;

trait ApiResponser{

    protected function successResponse($code = 200, $data = false)
	{
		if($data === false) {
			return response(null, $code);
		}
		else {
			return response()->json($data, $code);

		}
	}

	protected function errorResponse($code, $payload = false, $name = false, $description = false)
	{
		if($payload === false && $name === false && $description === false) {
			return response(null, $code);
		}
		else {
			return response()->json([
				'code' => $code,
				'payload' => $payload,
				'name' => $name,
				'description' => $description
			], $code);
		}
	}

}