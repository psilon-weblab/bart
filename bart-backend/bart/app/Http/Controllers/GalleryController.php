<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use App\Models\ImageModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;

class GalleryController extends Controller
{
	use ApiResponser;
	
	public function __construct()
	{
	    $this->storagePath = '/storage/galleries/';
        $this->Gallery = new Gallery($this->storagePath);
        $this->ImageModel = new ImageModel($this->storagePath);
	    
	}
	

    public function index()
    {
        $galleries = $this->Gallery->getAllGalleries();
        return $this->successResponse(200, $galleries);
    }

    public function store(Request $request) 
    {

    	$input = $request->all();
    	if(empty($input['name'])) {
    		return $this->errorResponse(
    			400,
    			[
    				'paths' => ['name'], 
    				'validator' => 'required', 
    				'example' => null
    			], 
    			'INVALID_SCHEMA', 
    			"Bad JSON object: u'name' is a required property",
    		);
    	}
    	if(strpos($input['name'], '/')) {
    		return $this->errorResponse(
    			400,
    			[
    				'paths' => ['name'], 
    				'validator' => 'required', 
    				'example' => null
    			], 
    			'INVALID_SCHEMA', 
    			"Bad JSON object: u'name' name must not contain a slash"
				
    		);
    	}

		if($this->Gallery->exists($input['name'])) {
			return $this->errorResponse(409);
		}

		//try {
			$gallery = $this->Gallery->store($input['name']);
		/*} catch (Exception $e) {
			return $this->errorResponse(500);
		}*/
		
		
		if(empty($gallery)) {
			return $this->errorResponse(500);
		}
		else {
			return $this->successResponse(201, $gallery);
		}

    }

    public function show($path) {
    	if(empty($path)) {
    		return $this->errorResponse(404);
    	}
    	$gallery = false;
    	try {
    		$gallery = $this->Gallery->findGallery($path);	
    	} catch (Exception $e) {
            return $this->errorResponse(500);
    	}
    	

    	if(!empty($gallery) || is_array($gallery)) {
    		return $this->successResponse(201, $gallery);
    	}
    	else {
    		return $this->errorResponse(404);
    	}
    }

    public function upload(Request $request, $path) {
        $keys = $request->all();
        if(empty($keys)) {
            return $this->errorResponse(400);
        }
        if(empty($path) || !$this->Gallery->exists($path, true)) {
            return $this->errorResponse(404);   
        }

        $files = ['uploaded' => []];
        foreach (array_keys($keys) as $key) {
            $file = $request->file($key);
           // return get_class($file);
            $uploadedFile = $file->storeAs($path, $file->getClientOriginalName());
            if($uploadedFile === false) {
                 return $this->errorResponse(400);
            }
            
            $imageFile = new \Symfony\Component\Finder\SplFileInfo(base_path() . $this->storagePath.$uploadedFile, $path, $file->getClientOriginalName());
            
            $files['uploaded'][] = $this->ImageModel->getObject($imageFile, $path);
        }


        return $this->successResponse(201, $files);

    }

    public function getImage(string $size, string $path, string $imageName) 
    {
     
        $path = urlencode($path);
        $imagepth = $path.'/'.$imageName;
        $size = explode('x', $size);
        $sizeW = (int) $size[0];
        $sizeH = (int) $size[1];
        
        if(empty($imagepth)) {
            return $this->errorResponse(404);
        }
        $manager = new ImageManager(array('driver' => 'gd'));
        $image = $manager->make($this->ImageModel->getImageLocation($imagepth));

        if($sizeW == 0 && $sizeH == 0) {
            return $image->response();
        }
        elseif($sizeW == 0) {
            $image->heighten($sizeH);
        }
        elseif($sizeH == 0) {
            $image->widen($sizeW);
        }
        else {
            $image->fit($sizeW, $sizeH);    
        }
        
        return $image->response();

    }

    public function delete(string $path, string $imageName = '') {

        if(!empty($imageName)) {
            $path = $path.'/'.$imageName;
            if(!$this->ImageModel->exists($path)) {
                return $this->errorResponse(404);
            }
            try {
                if(!$this->ImageModel->delete($path)) {
                    return $this->errorResponse(500);   
                };    
            } catch (Exception $e) {
                return $this->errorResponse(500);   
            }
            
            
        }
        else {
            if(!$this->Gallery->exists($path, true)) {
                return $this->errorResponse(404);
            }
            try {
                if(!$this->Gallery->delete($path)) {
                    return $this->errorResponse(500);   
                };    
            } catch (Exception $e) {
                return $this->errorResponse(500);   
            }
            
        }

        return $this->successResponse(200);
    }


}
