<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
Use App\Http\Controllers\GalleryController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('gallery', 'App\Http\Controllers\GalleryController@index');

Route::post('gallery', 'App\Http\Controllers\GalleryController@store');
 
Route::get('gallery/{path}', 'App\Http\Controllers\GalleryController@show');


Route::post('gallery/{path}', 'App\Http\Controllers\GalleryController@upload');

Route::get('images/{size}/{path}/{imageName}', 'App\Http\Controllers\GalleryController@getImage');

Route::delete('gallery/{path}', 'App\Http\Controllers\GalleryController@delete');

Route::delete('gallery/{path}/{imageName}', 'App\Http\Controllers\GalleryController@delete');
