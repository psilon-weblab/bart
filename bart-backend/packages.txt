ii  libapache2-mod-php                            2:7.4+79+ubuntu18.04.1+deb.sury.org+3                     all          server-side, HTML-embedded scripting language (Apache 2 module) (default)
ii  libapache2-mod-php5.6                         5.6.40-35+ubuntu18.04.1+deb.sury.org+1                    amd64        server-side, HTML-embedded scripting language (Apache 2 module)
ii  libapache2-mod-php7.2                         7.2.34-4+ubuntu18.04.1+deb.sury.org+1                     amd64        server-side, HTML-embedded scripting language (Apache 2 module)
ii  libapache2-mod-php7.4                         7.4.11-6+ubuntu18.04.1+deb.sury.org+1                     amd64        server-side, HTML-embedded scripting language (Apache 2 module)
ii  php                                           2:7.4+79+ubuntu18.04.1+deb.sury.org+3                     all          server-side, HTML-embedded scripting language (default)
ii  php-cli                                       2:7.4+79+ubuntu18.04.1+deb.sury.org+3                     all          command-line interpreter for the PHP scripting language (default)
ii  php-cli-prompt                                1.0.3+dfsg-1                                              all          tiny helper prompting for user input
ii  php-common                                    2:79+ubuntu18.04.1+deb.sury.org+3                         all          Common files for PHP packages
ii  php-composer-ca-bundle                        1.1.0-1                                                   all          utility library to find a path to the system CA bundle
ii  php-composer-semver                           1.4.2-1                                                   all          utilities, version constraint parsing and validation
ii  php-composer-spdx-licenses                    1.3.0-1                                                   all          SPDX licenses list and validation library
ii  php-curl                                      2:7.4+79+ubuntu18.04.1+deb.sury.org+3                     all          CURL module for PHP [default]
ii  php-gd                                        2:7.4+79+ubuntu18.04.1+deb.sury.org+3                     all          GD module for PHP [default]
ii  php-json                                      2:7.4+79+ubuntu18.04.1+deb.sury.org+3                     all          JSON module for PHP [default]
ii  php-json-schema                               5.2.6-1                                                   all          implementation of JSON schema
ii  php-mbstring                                  2:7.4+79+ubuntu18.04.1+deb.sury.org+3                     all          MBSTRING module for PHP [default]
ii  php-mysql                                     2:7.4+79+ubuntu18.04.1+deb.sury.org+3                     all          MySQL module for PHP [default]
ii  php-pear                                      1:1.10.12+submodules+notgz-1+ubuntu18.04.1+deb.sury.org+1 all          PEAR Base System
ii  php-pgsql                                     2:7.4+79+ubuntu18.04.1+deb.sury.org+3                     all          PostgreSQL module for PHP [default]
ii  php-psr-log                                   1.0.2-1                                                   all          common interface for logging libraries
ii  php-sqlite3                                   2:7.4+79+ubuntu18.04.1+deb.sury.org+3                     all          SQLite3 module for PHP [default]
ii  php-symfony-console                           3.4.6+dfsg-1ubuntu0.1                                     all          run tasks from the command line
ii  php-symfony-debug                             3.4.6+dfsg-1ubuntu0.1                                     all          tools to make debugging of PHP code easier
ii  php-symfony-filesystem                        3.4.6+dfsg-1ubuntu0.1                                     all          basic filesystem utilities
ii  php-symfony-finder                            3.4.6+dfsg-1ubuntu0.1                                     all          find files and directories
ii  php-symfony-polyfill-mbstring                 1.6.0-2                                                   all          Symfony polyfill for the Mbstring extension
ii  php-symfony-process                           3.4.6+dfsg-1ubuntu0.1                                     all          execute commands in sub-processes
ii  php-xml                                       2:7.4+79+ubuntu18.04.1+deb.sury.org+3                     all          DOM, SimpleXML, WDDX, XML, and XSL module for PHP [default]
ii  php-xmlrpc                                    2:7.4+79+ubuntu18.04.1+deb.sury.org+3                     all          XMLRPC-EPI module for PHP [default]
ii  php-zip                                       2:7.4+79+ubuntu18.04.1+deb.sury.org+3                     all          Zip module for PHP [default]
ii  php5.6                                        5.6.40-35+ubuntu18.04.1+deb.sury.org+1                    all          server-side, HTML-embedded scripting language (metapackage)
ii  php5.6-cli                                    5.6.40-35+ubuntu18.04.1+deb.sury.org+1                    amd64        command-line interpreter for the PHP scripting language
ii  php5.6-common                                 5.6.40-35+ubuntu18.04.1+deb.sury.org+1                    amd64        documentation, examples and common module for PHP
ii  php5.6-gd                                     5.6.40-35+ubuntu18.04.1+deb.sury.org+1                    amd64        GD module for PHP
ii  php5.6-json                                   5.6.40-35+ubuntu18.04.1+deb.sury.org+1                    amd64        JSON module for PHP
ii  php5.6-mysql                                  5.6.40-35+ubuntu18.04.1+deb.sury.org+1                    amd64        MySQL module for PHP
ii  php5.6-opcache                                5.6.40-35+ubuntu18.04.1+deb.sury.org+1                    amd64        Zend OpCache module for PHP
ii  php5.6-readline                               5.6.40-35+ubuntu18.04.1+deb.sury.org+1                    amd64        readline module for PHP
ii  php5.6-xml                                    5.6.40-35+ubuntu18.04.1+deb.sury.org+1                    amd64        DOM, SimpleXML, WDDX, XML, and XSL module for PHP
ii  php7.2                                        7.2.34-4+ubuntu18.04.1+deb.sury.org+1                     all          server-side, HTML-embedded scripting language (metapackage)
ii  php7.2-cli                                    7.2.34-4+ubuntu18.04.1+deb.sury.org+1                     amd64        command-line interpreter for the PHP scripting language
ii  php7.2-common                                 7.2.34-4+ubuntu18.04.1+deb.sury.org+1                     amd64        documentation, examples and common module for PHP
ii  php7.2-curl                                   7.2.34-4+ubuntu18.04.1+deb.sury.org+1                     amd64        CURL module for PHP
ii  php7.2-gd                                     7.2.34-4+ubuntu18.04.1+deb.sury.org+1                     amd64        GD module for PHP
ii  php7.2-json                                   7.2.34-4+ubuntu18.04.1+deb.sury.org+1                     amd64        JSON module for PHP
ii  php7.2-mbstring                               7.2.34-4+ubuntu18.04.1+deb.sury.org+1                     amd64        MBSTRING module for PHP
ii  php7.2-mysql                                  7.2.34-4+ubuntu18.04.1+deb.sury.org+1                     amd64        MySQL module for PHP
ii  php7.2-opcache                                7.2.34-4+ubuntu18.04.1+deb.sury.org+1                     amd64        Zend OpCache module for PHP
ii  php7.2-pgsql                                  7.2.34-4+ubuntu18.04.1+deb.sury.org+1                     amd64        PostgreSQL module for PHP
ii  php7.2-readline                               7.2.34-4+ubuntu18.04.1+deb.sury.org+1                     amd64        readline module for PHP
ii  php7.2-sqlite3                                7.2.34-4+ubuntu18.04.1+deb.sury.org+1                     amd64        SQLite3 module for PHP
ii  php7.2-xml                                    7.2.34-4+ubuntu18.04.1+deb.sury.org+1                     amd64        DOM, SimpleXML, WDDX, XML, and XSL module for PHP
ii  php7.2-xmlrpc                                 7.2.34-4+ubuntu18.04.1+deb.sury.org+1                     amd64        XMLRPC-EPI module for PHP
ii  php7.2-zip                                    7.2.34-4+ubuntu18.04.1+deb.sury.org+1                     amd64        Zip module for PHP
ii  php7.4                                        7.4.11-6+ubuntu18.04.1+deb.sury.org+1                     all          server-side, HTML-embedded scripting language (metapackage)
ii  php7.4-cli                                    7.4.11-6+ubuntu18.04.1+deb.sury.org+1                     amd64        command-line interpreter for the PHP scripting language
ii  php7.4-common                                 7.4.11-6+ubuntu18.04.1+deb.sury.org+1                     amd64        documentation, examples and common module for PHP
ii  php7.4-curl                                   7.4.11-6+ubuntu18.04.1+deb.sury.org+1                     amd64        CURL module for PHP
ii  php7.4-gd                                     7.4.11-6+ubuntu18.04.1+deb.sury.org+1                     amd64        GD module for PHP
ii  php7.4-json                                   7.4.11-6+ubuntu18.04.1+deb.sury.org+1                     amd64        JSON module for PHP
ii  php7.4-mbstring                               7.4.11-6+ubuntu18.04.1+deb.sury.org+1                     amd64        MBSTRING module for PHP
ii  php7.4-mysql                                  7.4.11-6+ubuntu18.04.1+deb.sury.org+1                     amd64        MySQL module for PHP
ii  php7.4-opcache                                7.4.11-6+ubuntu18.04.1+deb.sury.org+1                     amd64        Zend OpCache module for PHP
ii  php7.4-pgsql                                  7.4.11-6+ubuntu18.04.1+deb.sury.org+1                     amd64        PostgreSQL module for PHP
ii  php7.4-readline                               7.4.11-6+ubuntu18.04.1+deb.sury.org+1                     amd64        readline module for PHP
ii  php7.4-sqlite3                                7.4.11-6+ubuntu18.04.1+deb.sury.org+1                     amd64        SQLite3 module for PHP
ii  php7.4-xml                                    7.4.11-6+ubuntu18.04.1+deb.sury.org+1                     amd64        DOM, SimpleXML, XML, and XSL module for PHP
ii  php7.4-xmlrpc                                 7.4.11-6+ubuntu18.04.1+deb.sury.org+1                     amd64        XMLRPC-EPI module for PHP
ii  php7.4-zip                                    7.4.11-6+ubuntu18.04.1+deb.sury.org+1                     amd64        Zip module for PHP
